// Header js

const $menu = $('.menu');

$('.toggle').on('click', () => {
	$('#links').toggleClass('visible');
});

$(window).on('click', function(e) {
	if (!$menu.is(e.target) && $menu.has(e.target).length === 0){
		$('#links').removeClass('visible');
	}
})

// Stringer form js
$(function(){
	
	$('input[name="delivery"]').change(function(){
		if ($(this).val() != 0){
			$('#delivery_range').removeClass('hidden');
		} else {
			$('#delivery_range').addClass('hidden');
		}
	});
	
	$('input[name="has_strings"]').change(function(){
		if ($(this).val() != 0){
			$('#strings').removeClass('hidden');
		} else {
			$('#strings').addClass('hidden');
		}
	});
	
	$('#string_container_add').click(function(){
		var html = $('#example_string').clone()
		html.removeClass('hidden');
		$('#string_container').append(html);
	});
	
	$('#string_container').on('click', '.string_container_remove', function(){
		$(this).closest('#example_string').remove();
	});
});

// Address validation
$(function(){
	$('form#addressForm, form#customerEdit').submit(function(e){
		
		e.preventDefault();
		var form = this;
		
		$.post(
			"/customers/ajax/validateAddress",
			{
				zipcode: $('#zipcode').val(),
				street: $('#street').val(),
				housenumber: $('#housenumber').val(),
				housenumber_additional: $('#housenumber_additional').val(),
				city: $('#city').val()
			},
			function(data, status){
				if (data == "false"){
					$('#messages').append(
						'<div class="alert alert-danger" role="alert">\
							Adres is niet gevonden.\
						</div>'
					);
					return false;
				}
				
				data = jQuery.parseJSON(data);
				
				$('form#'+form.id).append(
					'\
					<input class="hidden" type="text" name="x" value="'+ data[0] +'"/>\
					<input class="hidden" type="text" name="y" value="'+ data[1] +'"/>\
					<input class="hidden" type="text" name="lat" value="'+ data[2] +'"/>\
					<input class="hidden" type="text" name="lng" value="'+ data[3] +'"/>\
					'
				);
				
				form.submit();
			}
		);
	});
});