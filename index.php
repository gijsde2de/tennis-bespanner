<?php

require "autoload.php";

use App\Db\Database;
use App\Request\Request;
use App\Session\Session;
use Setup\DbSchema;

$db = Database::getConnection();
Request::init();

// Filter out ajax/api calls before starting session
Session::init();

// setup url
if (Request::getParam('setup')){
	$setup = new DbSchema();
	$setup->upgrade();
	echo "Setup succesvol uitgevoerd";
} else {
	Request::execute();
}