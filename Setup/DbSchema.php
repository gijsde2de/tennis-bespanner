<?php

namespace Setup;

use App\Db\Database;

class DbSchema
{
	private $connection;
	
	public function __construct(){
		$this->connection = Database::getConnection();
	}
	
	public function upgrade(){
		$currentVersion = $this->getVersion();
		$newVersion = 0;
		
		if ($currentVersion < 1){
			$sql = "CREATE TABLE setup(id INT(1) AUTO_INCREMENT PRIMARY KEY, version INT)";
			$this->connection->query($sql);
			
			$sql = "INSERT INTO setup (version) VALUES (1)";
			$this->connection->query($sql);
			
			$newVersion = 1;
		}
		
		if ($currentVersion < 2){
			$sql = "CREATE TABLE customers(
				id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				firstname VARCHAR(30),
				lm_prefix VARCHAR(15),
				lastname VARCHAR(30),
				email VARCHAR(50),
				created_at DATETIME,
				updated_at DATETIME,
				password VARCHAR(100),
				is_stringer BOOL,
				INDEX email (email)
			)";
			
			$this->connection->query($sql);
			
			$sql = "CREATE TABLE customers_address(
				id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				user_id INT(10) UNSIGNED,
				zipcode VARCHAR(6),
				housenumber VARCHAR(6),
				housenumber_additional VARCHAR(6),
				street VARCHAR(50),
				city VARCHAR(30),
				x DECIMAL(10, 4),
				y DECIMAL(10, 4),
				lng DECIMAL(9, 6),
				lat DECIMAL(9, 6),
				INDEX user_id (user_id)
			)";
			
			$this->connection->query($sql);
			
			$sql = "CREATE TABLE customers_stringers(
				id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				user_id INT(10) UNSIGNED,
				price DECIMAL(10,4),
				duration INT(5),
				delivery BOOL,
				distance INT(10),
				delivery_price DECIMAL(10, 4),
				has_strings BOOL,
				INDEX delivery (delivery),
				INDEX duration (duration),
				INDEX price (price),
				INDEX user_id (user_id)
			)";
			
			$this->connection->query($sql);
			
			$sql = "CREATE TABLE customers_stringers_strings(
				id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				user_id INT(10) UNSIGNED,
				brand INT(5),
				material INT(5),
				type INT(5),
				thickness INT(5),
				string_price DECIMAL(10, 4),
				INDEX user_id (user_id)
			)";
			
			$this->connection->query($sql);
			
			$newVersion = 2;
		}
		
		if ($currentVersion < 3){
			$sql = "CREATE TABLE url_rewrite(
				id INT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				request VARCHAR(255),
				path VARCHAR(255)
			)";
			
			$this->connection->query($sql);
			
			$sql = "INSERT INTO url_rewrite (request, path) VALUES 
				('/', 'cms/index/index/page/home'),
				('/login', 'customers/login/index/page/login'),
				('/registreren', 'customers/register/index/page/register'),
				('/account', 'customers/account/index/page/index'),
				('/wachtwoord-vergeten', 'customers/forgot/index/page/forgot'),
				('/instellingen', 'customers/settings/index/page/settings'),
				('/bespanner-instellingen', 'customers/stringer/edit/page/edit'),
				('/account/wachtwoord', 'customers/account/password'),
				('/account/informatie', 'customers/account/information'),
				('/account/adres', 'customers/account/address')
			";
			$this->connection->query($sql);
			
			$newVersion = 3;
		}
		
		if ($newVersion > $currentVersion){
			$sql = "UPDATE setup SET version=". $newVersion;
			$this->connection->query($sql);
		}
	}
	
	public function getVersion(){
		$sql = "SELECT * FROM setup";
		$version = $this->connection->query($sql);
		
		// Basic setup has not been done
		if (!$version){
			$version = 0;
		} else {
			$version = $version->fetch_assoc()['version'];
		}
		
		return $version;
	}
}