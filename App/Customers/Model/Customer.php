<?php

namespace App\Customers\Model;

use App\Abstracts\AbstractModel;
use App\Customers\Model\Address;
use App\Customers\Model\Stringer;
use App\Db\Database;

class Customer extends AbstractModel
{
	public function __construct(){
		parent::__construct();
		$this->table = "customers";
	}

	public function save(){
		$password = $this->data['password'];
		$data = Database::escapeAll($this->data);

		if (isset($this->data['id'])){
			$sql = "
				UPDATE ". $this->table ."
				SET
					firstname = '". $data['firstname'] ."',
					lm_prefix = '". $data['lm_prefix'] ."',
					lastname = '". $data['lastname'] ."',
					email = '". $data['email'] ."',
					updated_at = NOW(),
					password = '". $data['password'] ."'
				WHERE id = ". $data['id'];
				
			$result = $this->connection->query($sql);
			
		} else {
			$encryptedPassword = crypt($password, '$2y$07$z3cj1ywo5923dmnw9d82wa$');
			$sql = "
			INSERT INTO ". $this->table ." 
				(firstname, lm_prefix, lastname, email, created_at, updated_at, password)
				VALUES (
					'". $data['firstname'] ."',
					'". $data['lm_prefix'] ."',
					'". $data['lastname'] ."',
					'". $data['email'] ."',
					NOW(),
					NOW(),
					'". $encryptedPassword ."'
				)
			";
			
			$result = $this->connection->query($sql);
			
			$this->data['id'] = $this->connection->insert_id;
		}
	}
	
	public function load(int $id){
		
		parent::load($id);

		$this->addAddress();
	
		$this->addStringer();
		
		return $this;
	}
	
	public function loadBy(string $key, $value){
		
		parent::loadBy($key, $value);
		
		if (isset($this->data['id'])){
			$this->addAddress();
		}
		
		if (isset($this->data['is_stringer']) && $this->data['is_stringer']){
			$this->addStringer();
		}
		
		return $this;
	}
	
	public function addAddress(){
		$address = new Address();
		$address->loadBy('user_id', $this->data['id']);
		$this->data['address'] = $address;
	}
	
	public function addStringer(){
		$stringer = new Stringer();
		$stringer->loadBy('user_id', $this->data['id']);
		$this->data['stringer'] = $stringer;
	}
	
	public function login(){
		$_SESSION['user_id'] = $this->data['id'];
	}
	
	public function logout(){
		session_unset();
		session_destroy();
	}
}