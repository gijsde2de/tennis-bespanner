<?php

namespace App\Customers\Model;

use App\Abstracts\AbstractModel;
use App\Db\Database;
use App\Services\Geocoding;

class Address extends AbstractModel
{
	public function __construct(){
		parent::__construct();
		$this->table = "customers_address";
	}

	public function save(){

		$data = Database::escapeAll($this->data);

		if (isset($data['id'])){
			// update
			$sql = "
				UPDATE ". $this->table ."
				SET
					zipcode = '". str_replace(' ', '', $data['zipcode']) ."',
					housenumber = '". $data['housenumber'] ."',
					housenumber_additional = '". $data['housenumber_additional'] ."',
					street = '". $data['street'] ."',
					city = '". $data['city'] ."',
					x = ". $data['x'] .",
					y = ". $data['y'] .",
					lat = ". $data['lat'] .",
					lng = ". $data['lng'] ."
				WHERE id = ". $data['id'];
				
			$result = $this->connection->query($sql);
		} else {
			// insert
			$sql = "
			INSERT INTO ". $this->table ." 
				(user_id, zipcode, housenumber, housenumber_additional, street, city, x, y, lat, lng)
				VALUES (
					'". $_SESSION['user_id'] ."',
					'". str_replace(' ', '', $data['zipcode']) ."',
					'". $data['housenumber'] ."',
					'". $data['housenumber_additional'] ."',
					'". $data['street'] ."',
					'". $data['city'] ."',
					". $data['x'] .",
					". $data['y'] .",
					". $data['lat'] .",
					". $data['lng'] ."
				)
			";
			
			$result = $this->connection->query($sql);
			
			$this->data['id'] = $this->connection->insert_id;
		}
	}
	
	public function findClose($x, $y, int $range = 20){	
		$sql = 	"SELECT * FROM 
					(SELECT user_id, lat, lng, SQRT((x - ". $x .")*(x - ". $x .") + (y - ". $y .")*(y - ". $y .")) as distance FROM customers_address) AS innertable
				LEFT JOIN customers ON customers.id = innertable.user_id
				WHERE distance < ". $range ." AND is_stringer = 1";
				
		$result = $this->connection->query($sql);

		if ($result->num_rows){
			$closeCustomers = $result->fetch_all(MYSQLI_ASSOC);
		} else {
			$closeCustomers = [];
		}
		
		return $closeCustomers;
	}
}