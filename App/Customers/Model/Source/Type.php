<?php

namespace App\Customers\Model\Source;

class Type
{
	public function toOptionArray(){
		return [
			0 => 'Allround',
			1 => 'Comfort',
			2 => 'Controle',
			3 => 'Duurzaamheid',
			4 => 'Power',
			5 => 'Spin'
		];
	}
}