<?php

namespace App\Customers\Model\Source;

class Material
{
	public function toOptionArray(){
		return [
			0 => 'Darm',
			1 => 'Hybride',
			2 => 'Multifilament',
			3 => 'Nylon',
			4 => 'Polyester'
		];
	}
}