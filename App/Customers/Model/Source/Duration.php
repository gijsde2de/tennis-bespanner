<?php

namespace App\Customers\Model\Source;

class Duration
{
	public function toOptionArray(){
		return [
			0 => '< 1 dag',
			1 => '1 dag',
			2 => '2 dagen',
			3 => '3 dagen',
			4 => '4 dagen',
			5 => '5 dagen'
		];
	}
}