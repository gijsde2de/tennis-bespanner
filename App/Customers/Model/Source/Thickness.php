<?php

namespace App\Customers\Model\Source;

class Thickness
{
	public function toOptionArray(){
		return [
			0 => '1.10 - 1.20mm',
			1 => '1.20 - 1.25mm',
			2 => '1.25 - 1.30mm',
			3 => '1.30 - 1.35mm',
			4 => '+ 1.35mm'
		];
	}
}