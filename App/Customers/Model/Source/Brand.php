<?php

namespace App\Customers\Model\Source;

class Brand
{
	public function toOptionArray(){
		return [
			0 => 'Babolat',
			1 => 'Dunlop',
			2 => 'Head',
			3 => 'Iso-Speed',
			4 => 'Kirschbaum',
			5 => 'Luxilon',
			6 => 'MSV',
			7 => 'Penta',
			8 => 'Prince',
			9 => 'Signum Pro',
			10 => 'Solinco',
			11 => 'Tecnifibre',
			12 => 'Toalson',
			13 => 'Topspin',
			14 => 'V&ouml;lkl',
			15 => 'Wilson',
			16 => 'Yonex'
		];
	}
}