<?php

namespace App\Customers\Model;

use App\Abstracts\AbstractModel;
use App\Customers\Model\Address;
use App\Customers\Model\Strings;
use App\Db\Database;

class Stringer extends AbstractModel
{
	public function __construct(){
		parent::__construct();
		$this->table = "customers_stringers";
	}
	
	public function save(){
		$stringData = $this->formatStrings();
		$data = Database::escapeAll($this->data);
		
		// Save main stringer data, address and strings are saved separately
		if (isset($data['id'])){
			// update
			$sql = "
				UPDATE ". $this->table ."
				SET
					price = '". str_replace(',', '.', $data['price']) ."',
					duration = '". $data['duration'] ."',
					delivery = '". $data['delivery'] ."',
					distance = '". $data['distance'] ."',
					delivery_price = '". str_replace(',', '.', $data['delivery_price']) ."',
					has_strings = '". ($data['has_strings'] ? (count($stringData) > 0) : 0) ."'
				WHERE id = ". $data['id'];
				
			$result = $this->connection->query($sql);
		} else {
			// insert
			$sql = "
			INSERT INTO ". $this->table ." 
				(user_id, price, duration, delivery, distance, delivery_price, has_strings)
				VALUES (
					'". $_SESSION['user_id'] ."',
					'". str_replace(',', '.', $data['price']) ."',
					'". $data['duration'] ."',
					'". $data['delivery'] ."',
					'". $data['distance'] ."',
					'". str_replace(',', '.', $data['delivery_price']) ."',
					'". ($data['has_strings'] ? (count($stringData) > 0) : 0) ."'
				)
			";
			
			$result = $this->connection->query($sql);
			
			$this->data['id'] = $this->connection->insert_id;
		}
		
		$address = new Address();
		$address->loadBy('user_id', $_SESSION['user_id']);
		unset($data['id']);
		$address->setData($data);
		$address->save();
		
		$strings = new Strings();
		if ($data['has_strings']){
			$strings->setData($stringData);
		}
		$strings->save();
		
		// Lastly mark customer as a stringer
		$sql = "UPDATE customers SET is_stringer = 1";
		$this->connection->query($sql);
	}
	
	public function formatStrings(){
		$result = [];
		for($i = 1; $i < count($this->data['brand']); $i++){
			$result[$i-1] = [
				'brand' => $this->data['brand'][$i],
				'material' => $this->data['material'][$i],
				'type' => $this->data['type'][$i],
				'thickness' => $this->data['thickness'][$i],
				'string_price' => $this->data['string_price'][$i]
			];
		}
		
		unset($this->data['brand']);
		unset($this->data['material']);
		unset($this->data['type']);
		unset($this->data['thickness']);
		unset($this->data['string_price']);

		return $result;
	} 
	
	public function load(int $id){
		
		parent::load($id);
		
		if (isset($this->data['user_id'])){
			$this->addStrings();
		}
		
		return $this;
	}
	
	public function loadBy(string $key, $value){
		
		parent::loadBy($key, $value);
		
		if (isset($this->data['user_id'])){
			$this->addStrings();
		}
		
		return $this;
	}
	
	public function addStrings(){
		$strings = new Strings();
		$strings->loadBy('user_id', $this->data['user_id']);
		
		$this->data['strings'] = $strings;
	}
}