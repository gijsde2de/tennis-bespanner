<?php

namespace App\Customers\Model;

use App\Abstracts\AbstractModel;
use App\Db\Database;

class Strings extends AbstractModel
{
	public function __construct(){
		parent::__construct();
		$this->table = "customers_stringers_strings";
	}

	public function save(){
		
		$sql = "DELETE FROM ". $this->table ." WHERE user_id = '". $_SESSION['user_id'] ."'";
		$this->connection->query($sql);

		foreach ($this->data as $row){
			$data = Database::escapeAll($row);

			// insert
			$sql = "
			INSERT INTO ". $this->table ." 
				(user_id, brand, material, type, thickness, string_price)
				VALUES (
					'". $_SESSION['user_id'] ."',
					'". $data['brand'] ."',
					'". $data['material'] ."',
					'". $data['type'] ."',
					'". $data['thickness'] ."',
					'". str_replace(',', '.', $data['string_price']) ."'
				)
			";
		
			$result = $this->connection->query($sql);
		}
	}
	
	public function loadBy(string $key, $value){
		$sql = "SELECT * FROM ". $this->table ." WHERE ". $this->connection->escape_string($key) ." = '". $this->connection->escape_string($value) ."'";
		$result = $this->connection->query($sql);

		if ($result->num_rows){
			$this->data = $result->fetch_all(MYSQLI_ASSOC);
		} else {
			$this->data = [];
		}
		
		return $this;
	}
}