<?php

namespace App\Customers\Controllers;

use App\Cms\Controllers\IndexController;
use App\Request\Request;
use App\Session\Session;
use App\Customers\Model\Customer;

class ForgotController extends IndexController
{
	public function __construct(){
		$this->module = "customers";
	}
	
	public function forgot(){
		$params = Request::getParams();
		
		$customer = new Customer();
		$customer->loadBy('email', $params['email']);
		
		// Customer doens't exist, try again?
		if (is_null($customer->getData('id'))){
			Session::addMessage('error', 'Account met e-mailadres '. $params['email'] .' is niet gevonden, registreer <a href="'. $this->getUrl('customers/register/index/page/register') .'">hier</a>');
			$this->redirect('customers/forgot/index/page/forgot');
		}
		
		// Send email

		Session::addMessage('success', 'U ontvangt spoedig een e-mail met informatie over uw account');
		$this->redirect('/');
	}
}