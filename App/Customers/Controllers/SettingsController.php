<?php

namespace App\Customers\Controllers;

use App\Cms\Controllers\IndexController;
use App\Request\Request;
use App\Customers\Model\Customer;

class SettingsController extends IndexController
{
	public function __construct(){
		$this->module = "customers";
	}
}