<?php

namespace App\Customers\Controllers;

use App\Cms\Controllers\IndexController as ParentController;
use App\Request\Request;
use App\Session\Session;
use App\Customers\Model\Customer;

class RegisterController extends ParentController
{
	public function __construct(){
		$this->module = "customers";
	}
	
	public function save(){
		$params = Request::getParams();

		$customer = new Customer();
		$customer->loadBy('email', $params['email']);
		
		if (!is_null($customer->getData('id'))){
			// Redirect to login page with flash message
			Session::addMessage('error', 'Account met dit e-mailadres bestaat al.');
			$this->redirect('customers/register/index/page/register');
		}
		
		// Account doesn't exist yet so add it
		// TODO add validation
		$customer->setData($params);
		$customer->save();
		
		// Login customer and redirect to his account page with flash message
		$customer->login();
		Session::addMessage('success', 'Succesvol geregistreerd, welkom '. $customer->getData('firstname').'!');
		$this->redirect('customers/account/index/page/index');
	}
}