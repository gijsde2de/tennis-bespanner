<?php

namespace App\Customers\Controllers;

use App\Request\Request;
use App\Services\Geocoding;
use App\Customers\Model\Address;

class AjaxController
{
	public function validateAddress(){
		$params = Request::getParams();
		
		$zipcode = str_replace(' ', '', $params['zipcode']);
		if (isset($params['housenumber_additional'])){
			$address = urlencode($params['street']." ". $params['housenumber'] ." ". $params['housenumber_additional'] ." ". $params['city']);
		} else {
			$address = urlencode($params['street']." ". $params['housenumber'] ." ". $params['city']);
		}
		
		$geocoding = new Geocoding();
		$result = $geocoding->getGeocoding($address);

		if ($result){
			echo json_encode($result);
		} else {
			$result = $geocoding->getGeocoding($zipcode);
			if ($result){
				echo json_encode($result);
			} else {
				echo json_encode(false);
			}
		}
	}
	
	public function getCloseStringers(){
		$params = Request::getParams();
		
		$x = 0; $y = 0;
		if (isset($params['lat']) && isset($params['lng'])){
			$geocoding = new Geocoding();
			list($x, $y) = $geocoding->geoLocationToGrid($params['lat'], $params['lng']);
		} elseif (isset($params['x']) && isset($params['y'])) {
			$x = $params['x'];
			$y = $params['y'];
		}
		
		$address = new Address();
		
		echo json_encode($address->findClose($x, $y));
	}
}