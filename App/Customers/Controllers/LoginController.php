<?php

namespace App\Customers\Controllers;

use App\Cms\Controllers\IndexController;
use App\Request\Request;
use App\Session\Session;
use App\Customers\Model\Customer;

class LoginController extends IndexController
{
	public function __construct(){
		$this->module = "customers";
	}
	
	public function login(){
		$params = Request::getParams();
		
		$encryptedPassword = crypt($params['password'], '$2y$07$z3cj1ywo5923dmnw9d82wa$');
		
		$customer = new Customer();
		$customer->loadBy('email', $params['email']);

		// Wrong info, send back to login
		if ($customer->getData('password') != $encryptedPassword){
			if(is_null($customer->getData('id'))){
				Session::addMessage('error', 'Account met dit e-mailadres bestaat niet, registreer <a href="'. $this->getUrl('customers/register/index/page/register') .'">hier</a>.');
			} else {
				Session::addMessage('error', 'Verkeerd wachtwoord, bent u uw <a href="'. $this->getUrl('customers/forgot/index/page/forgot') .'">wachtwoord vergeten</a>?');
			}
			$this->redirect('customers/login/index/page/login');
		}
		
		$customer->login();
		Session::addMessage('success', 'Succesvol ingelogd, welkom '. $customer->getData('firstname').'!');
		
		$this->redirect('customers/account/index/page/index');
	}
	
	public function logout(){
		if ($this->isLoggedIn()){
			$customer = new Customer();
			$customer->load($_SESSION['user_id']);
			$customer->logout();
		}
		$this->redirect('/');
	}
}