<?php

namespace App\Customers\Controllers;

use App\Cms\Controllers\IndexController as ParentController;
use App\Customers\Model\Address;
use App\Customers\Model\Customer;
use App\Request\Request;
use App\Session\Session;

class AccountController extends ParentController
{
	public function __construct(){
		$this->module = "customers";

		if (!isset($_SESSION['user_id'])){		
			$this->redirect('customers/login/index/page/login');
		}
	}
	
	public function password(){
		$params = Request::getParams();
		
		if (count($params)){
			if ($params['new_password'] == $params['new_password2']){
				$customer = new Customer();
				$customer->load($_SESSION['user_id']);
				
				$encryptedPassword = crypt($params['password'], '$2y$07$z3cj1ywo5923dmnw9d82wa$');
				
				if ($customer->getData('password') == $encryptedPassword){
					$encryptedPassword = crypt($params['new_password'], '$2y$07$z3cj1ywo5923dmnw9d82wa$');
					$customer->setData('password', $encryptedPassword);
					$customer->save();
					
					Session::addMessage('success', 'Wachtwoord is aangepast.');
					$this->redirect('customers/account/index/page/index');
					
				} else {
					Session::addMessage('error', 'Huidige wachtwoord is niet correct.');
					$this->redirect('customers/account/password');
				}
				
			} else {
				Session::addMessage('error', 'Nieuwe wachtwoord is niet hetzelfde.');
				$this->redirect('customers/account/password');
			}
		}
		
		$this->template = "View/". ucfirst($this->module) ."/MyAccountForms/passwordChange.phtml";
		$this->render();
	}
	
	public function information(){	
		$params = Request::getParams();
		
		if (count($params)){
			$customer = $this->getCustomer();
			if ($params['email'] != $customer->getData('email')){
				// Email has been changed so it shouldn't be in use already
				$customer2 = new Customer();
				$customer2->loadBy('email', $params['email']);
				if (!is_null($customer2->getData('id'))){
					Session::addMessage('error', 'E-mailadres is al in gebruik.');
					$this->redirect('customers/account/information');
				}
			}
			
			$customer->setData($params);
			$customer->save();
			
			Session::addMessage('success', 'Informatie is aangepast.');
			$this->redirect('customers/account/index/page/index');
		}
		
		$this->template = "View/". ucfirst($this->module) ."/MyAccountForms/information.phtml";
		$this->render();
	}
	
	public function address(){
		$params = Request::getParams();
		
		if (count($params)){
			$address = new Address();
			$address->loadBy('user_id', $_SESSION['user_id']);
			$address->setData($params);	
			$address->save();

			Session::addMessage('success', 'Adres is aangepast.');
			$this->redirect('customers/account/index/page/index');
		}
		
		$this->template = "View/". ucfirst($this->module) ."/MyAccountForms/address.phtml";
		$this->render();
	}
}