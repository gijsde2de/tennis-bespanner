<?php

namespace App\Customers\Controllers;

use App\Cms\Controllers\IndexController;
use App\Request\Request;
use App\Customers\Model\Stringer;
use App\Session\Session;

class StringerController extends IndexController
{
	public function __construct(){
		$this->module = "customers";
		$this->page = "edit";
	}
	
	public function edit(){
		$this->index();
	}
	
	public function save(){
		$params = Request::getParams();
		
		$userId = $_SESSION['user_id'];
		
		$stringer = new Stringer();
		$stringer->loadBy('user_id', $userId);
		$stringer->setData($params);
		$stringer->save();

		Session::addMessage('success', 'U hebt succesvol uw gegevens aangepast.');
		$this->redirect('customers/account/index/page/index');
	}
}