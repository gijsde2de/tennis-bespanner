<?php

namespace App\Services;

class Geocoding
{
	private $apiKey;
	
	public function __construct(){
		$this->apiKey = "AIzaSyBf9BS8vAipiEj7RPPit52wK66fHMLLwgA";
	}
	
	public function getGeocoding($address){
	
		$request = "https://maps.googleapis.com/maps/api/geocode/json?address=". $address ."&key=". $this->apiKey;

		$response = json_decode(file_get_contents($request), true);

		if (isset($response['results'][0]['geometry']['location'])){
			// Ingevulde latitude en longitude
			$lat = $response['results'][0]['geometry']['location']['lat'];
			$lng = $response['results'][0]['geometry']['location']['lng'];
			
			list($x, $y) = $this->geoLocationToGrid($lat, $lng);
			
			return [$x, $y, $lat, $lng];
		} else {
			return null;
		}
	}
	
	public function geoLocationToGrid($lat, $lng){
		// 0 punt latitude en longitude
		$baseLat = 52.0785723;
		$baseLng = 5.0906232;
		
		$R = 6371;
		$q1 = deg2rad($baseLat);
		$q2 = deg2rad($lat);
		$y1 = deg2rad($baseLng);
		$y2 = deg2rad($lng);
		
		$deltaQ = deg2rad($lat - $baseLat);
		$deltaY = deg2rad($lng - $baseLng);
		
		$a = sin($deltaQ/2) * sin($deltaQ/2) + cos($q1) * cos($q2) * sin($deltaY/2) * sin($deltaY/2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
		
		$d = $R * $c;
		
		$y = sin($y2 - $y1) * cos($q2);
		$x = cos($q1) * sin($q2) - sin($q1) * cos($q2) * cos($deltaY);
		
		$bearing = rad2deg(atan2($y, $x));
		$bearing = fmod(($bearing + 270), 360);
		
		$x = cos(deg2rad($bearing)) * $d;
		$y = sin(deg2rad($bearing)) * $d;
		
		return [$x, $y];
	}
}
