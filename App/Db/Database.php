<?php

namespace App\Db;

use mysqli;

class Database {

    private static $db;
    private $connection;

    private function __construct() {
        $this->connection = new mysqli("localhost", "root", "", "test");
		if ($this->connection->connect_error) {
			die("Connection failed: " . $this->connection->connect_error);
		}
    }

    function __destruct() {
        $this->connection->close();
    }

    public static function getConnection() {
        if (self::$db == null) {
            self::$db = new Database();
        }
        return self::$db->connection;
    }
	
	public static function escapeAll(array $data){
		$result = [];
		foreach ($data as $key => $value){
			if (is_array($value)){
				$result[$key] = self::escapeAll($value);
			} elseif (is_string($value)) {
				$result[$key] = self::$db->connection->escape_string($value);
			}
		}
		
		return $result;
	}
}

?>