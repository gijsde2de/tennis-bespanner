<?php

namespace App\Abstracts;

use App\Db\Database;

class AbstractModel
{
	protected $connection;
	protected $table;
	protected $data;
	
	public function __construct(){
		$this->connection = Database::getConnection();
		$this->data = [];
	}
	
	public function getData(string $key = null){
		if (is_null($key)){
			return $this->data;
		} elseif (isset($this->data[$key])){
			return $this->data[$key];
		} else {
			return null;
		}
	}
	
	public function setData($arg1, $arg2 = null){
		if (is_array($arg1)){
			$this->data = $arg1 + $this->data;
		} elseif (is_string($arg1)){
			$this->data[$arg1] = $arg2;
		}
	}
	
	public function load(int $id){
		$sql = "SELECT * FROM ". $this->table ." WHERE id = ". $this->connection->escape_string($id);
		$result = $this->connection->query($sql);
		
		if ($result->num_rows){
			$this->data = $result->fetch_assoc();
		} else {
			$this->data = [];
		}
		
		return $this;
	}
	
	public function loadBy(string $key, $value){
		$sql = "SELECT * FROM ". $this->table ." WHERE ". $this->connection->escape_string($key) ." = '". $this->connection->escape_string($value) ."' LIMIT 1";
		$result = $this->connection->query($sql);

		if ($result->num_rows){
			$this->data = $result->fetch_assoc();
		} else {
			$this->data = [];
		}
		
		return $this;
	}
}