<?php

namespace App\Session;

class Session
{
	private static $session;
	
	public function __construct(){
		session_start();
	}
	
	public static function init(){
		self::$session = new Session();
	}
	
	public static function addMessage(string $type, string $message){
		$_SESSION['flash_messages'][$type][] = $message;
	}
	
	public static function resetMessages(){
		$_SESSION['flash_messages'] = [];
	}
}