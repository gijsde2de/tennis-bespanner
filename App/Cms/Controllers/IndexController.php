<?php

namespace App\Cms\Controllers;

use App\Customers\Model\Customer;
use App\Request\Request;
use App\Session\Session;

class IndexController
{
	public $template;
	
	protected $module = 'cms';
	protected $page;
	
	protected $customer;
	
	public function index($page = null){
		if (is_null($page) && is_null($this->page)){
			$this->template = "View/Errors/404.phtml";
		} else if (!is_null($this->page)){
			$this->template = "View/". ucfirst($this->module) ."/". $this->page .".phtml";
		} else {
			$this->template = "View/". ucfirst($this->module) ."/". $page .".phtml";
		}
		
		$this->render();
	}
	
	public function render(){
		
		include("View/Cms/header.phtml");

		include($this->template);
		
		include("View/Cms/footer.phtml");
		
		// We're done rendering a full page so reset flash messages;
		Session::resetMessages();
	}
	
	public function redirect(string $url){
		$url = Request::getUrl($url);

		header("Location: ". $url);
		exit;
	}
	
	public function getUrl(string $path){
		return Request::getUrl($path);
	}
	
	public function isLoggedIn(){
		return isset($_SESSION['user_id']);
	}
	
	public function isStringer(){
		$customer = $this->getCustomer();
		return $customer->getData('is_stringer');
	}
	
	public function getMessages(){
		if (!isset($_SESSION['flash_messages'])){
			return [];
		}
		
		$types = [
			'success' => 'success',
			'warning' => 'warning',
			'error' => 'danger',
		];
		
		$messages = [];
		foreach ($_SESSION['flash_messages'] as $type => $message){
			$messages[$types[$type]] = implode('<br />', $message);
		}
		
		return $messages;
	}
	
	public function getSelectOptions(string $sourceModel){
		$source = new $sourceModel();
		return $source->toOptionArray();
	}
	
	public function getCustomer(){
		if (isset($this->customer)){
			return $this->customer;
		}
		
		if ($this->isLoggedIn()){
			$customer = new Customer();
			$customer->load($_SESSION['user_id']);
			
			$this->customer = $customer;
			return $this->customer;
		} else {
			return null;
		}
	}
	
	public function getLocation(){
		$customer = $this->getCustomer();
		
		if ($customer){
			$address = $customer->getData('address');
			if ($address->getData()){
				return [$address->getData('lat'), $address->getData('lng')];
			}
		}
		
		// If no address is entered attempt to get location by javascript in frontend
		return null;
	}
}