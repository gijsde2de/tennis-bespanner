<?php

namespace App\Request;

// Requests are formatted in the following way
// domain/moduleName/controllerName/functionName
// For instance localhost/cms/index/index would look in the CMS module for the index controller and call the index method.
class Request
{
	private static $url;
	private $request;
	private $params;
	private $router;
	
	private function __construct() {
		$this->request = explode('?', $_SERVER['REQUEST_URI'])[0];
		$this->router = new Router($this);
		
		$this->params = $this->extractParams($this->request);
	}
	
	public static function extractParams(string $request){
		$params = $_GET + $_POST;
		
		$parts = array_filter(explode('/', $request));
		array_splice($parts, 0, 3);
		
		$i = 0;
		while (true){
			if (!isset($parts[$i+1]))
				break;
			
			$key = $parts[$i];
			$value = $parts[$i+1];
			$params += [$key => $value];
			
			$i += 2;
		}

		return $params;
	}
	
	// Format the request for easy access
	public static function extractPath(string $request)
	{
		$parts = array_values(array_filter(explode('/', $request)));
		$result = [];
		if (isset($parts[0]))
			$result['module'] = $parts[0];
		else
			$result['module'] = 'cms';
		
		if (isset($parts[1]))
			$result['controller'] = $parts[1];
		else 
			$result['controller'] = 'index';
		
		if (isset($parts[2]))
			$result['method'] = $parts[2];
		else
			$result['method'] = 'index';
		
		return $result;
	}
	
	public static function init(){
		self::$url = new Request();
	}
	
	public static function execute(){
		self::$url->router->execute();
	}
	
	public static function getRequest(){
		return self::$url->request;
	}
	
	public static function getParams(){
		return self::$url->params;
	}
	
	public static function getParam($param){
		return (isset(self::$url->params[$param]) ? self::$url->params[$param] : null);
	}
	
	// Handle apache's/xampp port numbers
	public static function getUrl($path){
		if ($_SERVER['SERVER_PORT'] == "8080"){
			$url = 'http://'. $_SERVER['SERVER_NAME'] .":". $_SERVER['SERVER_PORT'] . self::$url->router->getUrl($path);
		} else {
			$url = 'http://'. $_SERVER['SERVER_NAME'] . self::$url->router->getUrl($path);
		}
		
		return $url;
	}
}