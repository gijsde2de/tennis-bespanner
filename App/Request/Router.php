<?php

namespace App\Request;

use App\Db\Database;

// Requests are formatted in the following way
// domain/moduleName/controllerName/functionName
// For instance localhost/cms/index/index would look in the CMS module for the index controller and call the index method.
class Router
{
	public function execute(){
		$request = Request::getRequest();
		$path = $this->getPath($request);
		
		$params = Request::extractParams($path);
		$path = Request::extractPath($path);
		
		$class = 'App\\'. ucfirst($path['module']) .'\\Controllers\\'. ucfirst($path['controller']) .'Controller';

		// Default fallback is cms controller which can show a 404 page
		if (!file_exists($class.'.php')){
			$class = 'App\\Cms\\Controllers\\IndexController';
		}

		$instance = new $class();

		call_user_func_array([$instance, $path['method']], array_values($params));
	}
	
	// Try and match a pretty url to the actual module/controller/function
	public function getPath(string $request){
		$connection = Database::getConnection();
		$sql = "SELECT * FROM url_rewrite WHERE request='". $connection->escape_string($request) ."' LIMIT 1";
		$result = $connection->query($sql);

		if ($result->num_rows){
			return $result->fetch_assoc()['path'];
		} else {
			return $request;
		}
	}
	
	
	public function getUrl(string $path){
		$connection = Database::getConnection();
		$sql = "SELECT * FROM url_rewrite WHERE path='". $connection->escape_string($path) ."' LIMIT 1";
		$result = $connection->query($sql);

		if ($result->num_rows){
			return $result->fetch_assoc()['request'];
		} else {
			return $path;
		}
	}
}