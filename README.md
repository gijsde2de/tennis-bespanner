Doel van het project:

Een soort marktplaats zijn voor tennis racket bespanners. Zij kunnen registreren en allerlei informatie invullen waarna ze vindbaar zijn voor klanten.


Requirements:

Ik heb dit project gemaakt met xampp voor apache/mysql, net nog geupdate naar laatste versie en code compatible gemaakt met PHP 8
Database gegevens staan standaard op:
Host: localhost
Database: test
username: root
wachtwoord: (leeg)

Deze kun je aanpassen in App/Db/Database.php

Verder is het belangrijk voor de routing dat het project in de hoofdfolder van je hosting staat. 
De .htaccess stuurt alles door naar de index.php in de root folder

Voor je het kunt gebruiken wil je de volgende url aanroepen (ervan uitgaand dat je op localhost zit) localhost/a/b/c/setup/1 
Dit maakt alle tabellen aan en vult wat dingen in, zie Setup/DbSchema.php
De link ziet er een beetje raar uit maar dat komt door de routing, standaard routing werkt zo dat a = module naam, b = controller naam en c = functie binnen die controller
De setup is een speciaal geval die in de index.php wordt afgevangen als de parameter setup = 1 meegestuurd wordt.
Dit betekend dat iedere url waarbij setup/1 na de eerste 3 waardes wordt meegestuurd de setup triggered.


Gebruikersinfo:

Na installatie heb je een lege omgeving, de homepagina heeft alleen een lege google maps ingeladen.
Nu kun je een account registreren en inloggen daarmee (worden geen mailtjes verstuurd en je mag onzin informatie invullen)
Met dit account kun je bespanner worden, daarvoor moet je adres bekent zijn (dit moet kloppen, zit een check in bij google api)
Nadat je daar de gegevens hebt ingevuld kun je uitloggen en zou je als het goed is op de ingelade map op het homescreen een pijltje bij het door jou gekozen adres moeten zien.
Dit is tot hoever het nu werkt, het idee is natuurlijk dat dit pijltje wordt uitgebreid met wat informatie over de bespanner, dus hoe duur die is, hoe snel etc etc.

Een technisch leuk puntje om te zien is de App/Services/Geocoding.php. Dit platform is bedoeld dat klanten makkelijk racket bespanners bij hun in de buurt kunnen vinden.
Om dat goed te doen is het belangrijk dat een afstand filter goed werkt. Om te voorkomen dat iedere keer als de map geladen wordt er x aantal api calls gedaan moeten worden
om de afstand tussen mensen uit te rekenen. Wordt in plaats daarvan bij het opslaan van je adres, je adres omgezet in een x,y coordinaat.
Dit is moeilijker dan verwacht omdat google longitude/latitude teruggeeft wat niet eenvoudig om te zetten is naar x,y in kilometers. Dat gebeurd dus in die geocoding.php
Nu bij ieder adres altijd de x,y coordinaat bekend is is het heel makkelijk om "closeDistance" adressen op te halen, een query daarvoor zie je in App/Customers/Model/Address.php

Bij vragen kan je altijd bellen/mailen/appen. En we zien elkaar Dinsdag.